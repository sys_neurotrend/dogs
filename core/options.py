#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import json
from core.models import Option

def get_option(name):
    opt = Option.objects.filter(name=name).all()
    for o in opt:
        typ = o.type
        if typ == Option.STRING:
            return o.value
        elif typ == Option.NUMBER:
            return float(o.value)
        elif typ == Option.JSON:
            return json.loads(o.value)
        elif typ == Option.FORMULA:
            return o.value
        else:
            return None

    # не нашлось
    return None