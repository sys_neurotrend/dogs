from django.core.exceptions import ValidationError
from core.parser.DAOIlib import DAOI

def validate_archive(filename):
    if not str(filename).endswith('.zip'):
        raise ValidationError(u'Неверный формат файла')


def validate_aoi(file):
    aoi = DAOI()
    try:
        res = aoi.validate_aoi_met(file.read().decode('utf-8'))
    except Exception as ex:
        raise ValidationError('Not read file: ' + str(ex))
    if aoi.error:
        raise ValidationError(aoi.error)
