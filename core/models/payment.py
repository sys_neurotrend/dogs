# coding=utf-8

from __future__ import unicode_literals
import os
import json
from django.db import models, transaction
from datetime import datetime, date
from django.conf import settings
from django.utils import timezone
from django.core.files.storage import FileSystemStorage
from django.contrib.postgres.fields import JSONField
from dogs.settings import *
from core.models import Trustee, Dog

import logging
logger = logging.getLogger(__name__)

null = {'null': True, 'blank': True, }

# данные, загружаемые в систему, полученные в результате проведения эксперимента
class Payment(models.Model):

    purpose = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name="Цель",
    )
    sum = models.FloatField(
        null=False,
        blank=False,
        verbose_name="Сумма",
    )
    time = models.DateTimeField(
        editable=True,
        verbose_name="Дата",
    )

    trustee = models.ForeignKey(
        Trustee,
        null=True,
        blank=True,
        verbose_name="Опекун",
    )

    dog = models.ForeignKey(
        Dog,
        null=True,
        blank=True,
        verbose_name="Собака",
    )


    class Meta:
        verbose_name_plural = '3. Платежи'
        verbose_name = 'Платеж'

    def __str__(self):
        return "%s. %s - %s" % (self.id, self.sum, self.purpose)