# coding=utf-8

from __future__ import unicode_literals
import os
import json
from django.db import models, transaction
from datetime import datetime, date
from django.conf import settings
from django.utils import timezone
from django.core.files.storage import FileSystemStorage
from django.contrib.postgres.fields import JSONField
from dogs.settings import *

import logging
logger = logging.getLogger(__name__)

null = {'null': True, 'blank': True, }

# хранилища для файлов
fs = FileSystemStorage(location=MEDIA_FOLDER)

def get_trustee_pic_path(instance, filename):
    url = "trustee/%s" % (filename)
    return os.path.join(url)

# данные, загружаемые в систему, полученные в результате проведения эксперимента
class Trustee(models.Model):

    fio = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name="ФИО",
    )
    birth_time = models.DateTimeField(
        editable=True,
        verbose_name="Дата рождения",
    )

    photo = models.FileField(
        upload_to=get_trustee_pic_path,
        storage=fs,
        null=True,
        blank=True,
        verbose_name="Фото",
    )


    class Meta:
        verbose_name_plural = '2. Опекуны'
        verbose_name = 'Опекун'

    def __str__(self):
        return "%s. %s" % (self.id, self.fio)