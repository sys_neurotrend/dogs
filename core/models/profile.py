# coding=utf-8

from __future__ import unicode_literals
import os
import json
from django.db import models, transaction
from datetime import datetime, date
from django.conf import settings
from django.utils import timezone
from django.core.files.storage import FileSystemStorage
from django.contrib.postgres.fields import JSONField
from django.contrib.auth.models import User
from dogs.settings import *

import logging
logger = logging.getLogger(__name__)

null = {'null': True, 'blank': True, }

# данные, загружаемые в систему, полученные в результате проведения эксперимента
class Profile(models.Model):

    user = models.ForeignKey(
        User,
        null=False,
        blank=False,
        verbose_name="Пользователь",
        related_name="profile",
        on_delete=models.CASCADE
    )

    fio = models.CharField(
        max_length=255,
        null=False,
        blank=False,
        verbose_name="ФИО",
    )

    class Meta:
        verbose_name_plural = '4. Профили'
        verbose_name = 'Профиль'

    def __str__(self):
        return "%s. %s" % (self.id, self.fio)