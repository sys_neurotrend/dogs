from django.utils import six
from django.contrib.auth.decorators import user_passes_test

from django.shortcuts import render
from django.contrib.auth import (
    authenticate,
    login,
    logout,
)
from django.http import (
    HttpResponse,
    JsonResponse,
)
from rest_framework.authtoken.models import Token
import logging
import pprint

from django.http import HttpResponseForbidden
from django.contrib.auth.decorators import user_passes_test

pprint = pprint.PrettyPrinter(indent=4).pprint
logger = logging.getLogger('all_to_file')

def json_response(data, status=200):
    return JsonResponse(
        data,
        status=status,
        content_type='application/json; encoding=utf-8',
    )


# декоратор, проверяющий что юзер залогинен. Если нет, возвращает Unauthorized
def auth_required(func):
    def _decorated(request, *args, **kwargs):
        if request.user and request.user.is_authenticated():
            return func(request, *args, **kwargs)
        else:
            return json_response({'status': 'ERROR', 'message': 'Unauthorized',}, 401)

    return _decorated

def group_required(*group_names):
    """Requires user membership in at least one of the groups passed in."""
    def in_groups(u):
        if u.is_authenticated():
            if bool(u.groups.filter(name__in=group_names)) | u.is_superuser:
                return True
        return False
    return user_passes_test(in_groups, login_url='/login')
