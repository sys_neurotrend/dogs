#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import viewsets, filters
from rest_framework.exceptions import (
    PermissionDenied,
    ValidationError,
)
import django_filters
from rest_framework.response import Response

from core.models import (
    Dog,
    Trustee,
    Payment,
)

from django.db.models import Q
from django.core.exceptions import ObjectDoesNotExist
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.permissions import IsAuthenticated
from collections import defaultdict
from core.api.serializers import (
    DogSerializer,
    TrusteeSerializer,
    PaymentSerializer,
)
from datetime import datetime
from rest_framework.renderers import JSONRenderer
import logging
logger = logging.getLogger(__name__)


class DogViewSet(viewsets.ModelViewSet):
    model = Dog
    queryset = Dog.objects.all()
    serializer_class = DogSerializer
    permission_classes = (IsAuthenticated,)

class TrusteeViewSet(viewsets.ModelViewSet):
    model = Trustee
    queryset = Trustee.objects.all()
    serializer_class = TrusteeSerializer
    permission_classes = (IsAuthenticated,)

class PaymentViewSet(viewsets.ModelViewSet):
    model = Payment
    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer
    permission_classes = (IsAuthenticated,)

