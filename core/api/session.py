from __future__ import absolute_import
from django.contrib.auth import (
    authenticate,
    login,
    logout,
)
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from rest_framework.decorators import api_view
from rest_framework.authtoken.models import Token
from django.views.decorators.http import require_http_methods
from core.decorators import (
    auth_required,
    json_response,
)
from .base import JSONView
import itertools
from dogs.settings import *

import logging
import pprint
pprint = pprint.PrettyPrinter(indent=4).pprint
logger = logging.getLogger('all_to_file')

@api_view(['GET', 'POST'])
def LoginView(request):
    username = request.POST.get('username') or \
                request.GET.get('username') or \
                request.data.get('username') or ''
    password = request.POST.get('password') or \
                request.GET.get('password') or \
                request.data.get('password') or ''
    username = username.lower()

    logger.error("login {}:{}".format(username, password))

    user = authenticate(
        username=username,
        password=password,
    )

    if user:
        login(request, user)

        # reuse existing token!
        tokens = Token.objects.filter(user=user).all()
        token = None
        if not tokens:
            token = Token.objects.create(user=user)
        else:
            token = tokens[0]

        resp = {
            'status':'OK',
            'message': 'Access granted',
            'token': token.key,
            'username': user.username,
            'user_id': user.id,
            'staff': user.is_staff,
            'superuser': user.is_superuser,
        }

        data = json_response(resp)
        return data
    else:
        return json_response(
            {'status':'ERROR', 'message': 'Incorrect login or password'},
            401
        )


class LogoutView(JSONView):

    def get(self, request, format=None):
        user = request.user
        Token.objects.filter(user=user).delete()
        logout(request)
        return self.response({'status':'OK', 'message': 'Logout success'})

    def post(self, request, format=None):
        user = request.user
        Token.objects.filter(user=user).delete()
        logout(request)
        return self.response({'status':'OK', 'message': 'Logout success'})
