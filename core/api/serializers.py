#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from rest_framework.exceptions import PermissionDenied
from django.db.models import Q
from collections import OrderedDict
from core.models import (
    Dog,
    Trustee,
    Payment,
)

import logging
logger = logging.getLogger('all_to_file')

class DogSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dog
        fields = (
            'id',
            'name',
            'kind',
            'trustee',
            'birth_time',
            'reg_time',
            'photo',
        )


class TrusteeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Trustee
        fields = (
            'id',
            'fio',
            'birth_time',
            'photo',
        )


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = (
            'id',
            'sum',
            'purpose',
            'time',
            'trustee',
            'dog',
        )
