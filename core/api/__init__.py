#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import routers
from core.api.viewsets import (
    DogViewSet,
    TrusteeViewSet,
    PaymentViewSet,
)
from core.api.session import (
    LoginView,
    LogoutView,
)

router = routers.DefaultRouter()

router.register(
    r'dog',
    DogViewSet,
    base_name='dog'
)

router.register(
    r'trustee',
    TrusteeViewSet,
    base_name='trustee'
)

router.register(
    r'payment',
    PaymentViewSet,
    base_name='payment'
)
