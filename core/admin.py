from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from advanced_filters.admin import AdminAdvancedFiltersMixin
from django.utils.translation import ugettext_lazy as _
from django import forms
from django.db import models
from core.models import (
    Dog,
    Trustee,
    Payment,
)


class BaseAdmin(admin.ModelAdmin):
    def delete_model(self, request, obj):
        obj.adminarea = True
        super().delete_model(request, obj)

    def save_model(self, request, obj, form, change):
        obj.adminarea = True
        super().save_model(request, obj, form, change)


class DogAdmin(BaseAdmin):
    fields = ['name', 'kind', 'birth_time', 'trustee', 'photo']
    list_display = ['id', 'name', 'kind', 'birth_time', 'trustee']
    list_display_links = ('id', 'name',)
    list_filter = (
        ('kind'),
    )
    search_fields = ['name']

admin.site.register(Dog, DogAdmin)


class TrusteeAdmin(BaseAdmin):
    fields = ['fio', 'birth_time', 'photo']
    list_display = ['id', 'fio', 'birth_time']
    list_display_links = ('id', 'fio',)
    search_fields = ['fio']

admin.site.register(Trustee, TrusteeAdmin)


class PaymentAdmin(BaseAdmin):
    fields = ['purpose', 'time', 'sum', 'trustee', 'dog']
    list_display = ['id', 'purpose', 'time', 'sum', 'trustee', 'dog']
    list_display_links = ('id', 'purpose',)
    search_fields = ['purpose']

admin.site.register(Payment, PaymentAdmin)


