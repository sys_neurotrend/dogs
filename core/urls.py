from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from django.views.generic import RedirectView, TemplateView
from core import views
from core.api import router
from core.api.session import (
    LoginView,
    LogoutView,
)

urlpatterns = [
    url(r'^api/', include(router.urls)),

    #SESSIONS
    url(r'^api/login/$', LoginView, name="login"),
    url(r'^api/logout/$', LogoutView.as_view(), name="logout"),
]

