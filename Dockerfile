FROM python:3.5

ENV PYTHONUNBUFFERED 1
ENV APP dogs
ENV APPDIR /app
ENV LOGDIR /var/log/app

RUN mkdir -p $APPDIR
RUN mkdir -p $LOGDIR

WORKDIR $APPDIR

# Use "bash" as replacement for "sh"
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN set -x && \
    apt-get update -y && \
    apt-get upgrade -y  && \
    apt-get install -y locales && \
    apt-get install -y python-dev libldap2-dev libsasl2-dev libssl-dev && \
    echo ru_RU.UTF-8 UTF-8 >> /etc/locale.gen && \
    locale-gen

# install static
ADD requirements*.txt ${APPDIR}/
RUN pip install -r requirements-dev.txt

ADD . ${APPDIR}/

RUN ./manage.py collectstatic --noinput
EXPOSE 80
CMD /usr/local/bin/gunicorn --chdir $APPDIR --workers 2 --timeout 120 $APP.wsgi:application -b :80




